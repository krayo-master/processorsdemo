package cz.seznam.fulltext.robot;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class RunnerTest {

    @Test
    public void testMainWithInvalidProcessor() {
        String[] args = {"InvalidProcessor"};
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> Runner.main(args));
        assertEquals("Processor defined in arguments is not allowed.", exception.getMessage());
    }

    @Test
    public void testMainWithGrepProcessorAndMissingRegexArg() {
        String[] args = {"Grep"};
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> Runner.main(args));
        assertEquals("This Processor requires second regex argument.", exception.getMessage());
    }

    @Test
    public void testCheckArgsWithNoArgs() {
        String[] args = {};

        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> Runner.checkArgs(args));
        assertEquals("Argument defining processor class is necessary.", exception.getMessage());
    }
//
    @Test
    public void testCheckArgsWithInvalidProcessor() {
        String[] args = {"InvalidProcessor"};

        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> Runner.checkArgs(args));
        assertEquals("Processor defined in arguments is not allowed.", exception.getMessage());
    }
//
    @Test
    public void testCheckArgsWithGrepProcessorAndMissingRegexArg() {
        String[] args = {"Grep"};

        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> Runner.checkArgs(args));
        assertEquals("This Processor requires second regex argument.", exception.getMessage());
    }
}
