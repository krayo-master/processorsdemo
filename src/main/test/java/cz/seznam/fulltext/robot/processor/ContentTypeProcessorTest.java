package cz.seznam.fulltext.robot.processor;

import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ContentTypeProcessorTest {

    @Test
    public void testProcessWithMockedInput() throws IOException
    {
        String input = "url1\tcontent-type1\t1\nurl2\tcontent-type2\t1\nurl3\tcontent-type1\t1\nurl4\tconte" +
                "nt-type3\t1\nurl5\tcontent-type2\t1\nurl6\tcontent-type3\t1\n";

        System.setIn(new ByteArrayInputStream(input.getBytes()));
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outputStream));

        new ContentTypeProcessor().process();

        String[] lines = outputStream.toString().split(System.lineSeparator());
        assertEquals("content-type1\t2", lines[0]);
        assertEquals("content-type3\t2", lines[2]);
    }
}
