package cz.seznam.fulltext.robot.processor;

import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TopProcessorTest {

    @Test
    public void testProcessWithMockedInput() throws IOException {
        String input = "url1\tcontent-type\t10\nurl2\tcontent-type\t20\nurl3\tcontent-type\t30\nurl4\tcontent-" +
                "type\t40\nurl5\tcontent-type\t50\nurl6\tcontent-type\t60\nurl7\tcontent-type\t70\nurl8\tcontent-" +
                "type\t80\nurl9\tcontent-type\t90\nurl10\tcontent-type\t100\nurl11\tcontent-type\t110\n";

        System.setIn(new ByteArrayInputStream(input.getBytes()));
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outputStream));

        new TopProcessor().process();

        String[] lines = outputStream.toString().split(System.lineSeparator());
        assertEquals("url11\t110", lines[0]);
        assertEquals("url2\t20", lines[9]);
    }
}
