package cz.seznam.fulltext.robot.processor;

import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class GrepProcessorTest {

    @Test
    public void testProcessWithMockedInput() throws IOException
    {
        String input = "Line 1\nLine 2: pattern\nLine 3\nLine 4: pattern\n";

        System.setIn(new ByteArrayInputStream(input.getBytes()));
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outputStream));

        new GrepProcessor().process("pattern");

        String[] lines = outputStream.toString().split(System.lineSeparator());
        assertEquals("Line 2: pattern", lines[0]);
        assertEquals("Line 4: pattern", lines[1]);
    }

    @Test
    public void testProcessWithNoMatchingInput() throws IOException {
        String input = "Line 1\nLine 2\nLine 3\nLine 4\n";

        System.setIn(new ByteArrayInputStream(input.getBytes()));
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outputStream));

        new GrepProcessor().process("pattern");

        assertEquals("", outputStream.toString());
    }
}
