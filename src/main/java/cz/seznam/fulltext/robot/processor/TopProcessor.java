package cz.seznam.fulltext.robot.processor;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.PriorityQueue;

public class TopProcessor implements SingleArgumentProcessor
{
    @Override
    public void process() throws IOException
    {
        PriorityQueue<UrlCountPair> queue = new PriorityQueue<>(Comparator.comparingInt(UrlCountPair::getCount));
        try(BufferedReader input = new BufferedReader(new InputStreamReader(System.in)))
        {

            String line;
            while ((line = input.readLine()) != null) {
                String[] parts = line.split("\t");
                String url = parts[0];
                int count = Integer.parseInt(parts[2]);
                queue.offer(new UrlCountPair(url, count));
                if (queue.size() > 10) {
                    queue.poll();
                }
            }
            printQueueContentFromTop(queue);
        }
    }

    private void printQueueContentFromTop(PriorityQueue<UrlCountPair> queue)
    {
        List<UrlCountPair> listToPrint = new ArrayList<>();
        while(!queue.isEmpty()) {
            listToPrint.add(queue.poll());
        }
        Collections.reverse(listToPrint);
        listToPrint.forEach(System.out::println);
    }

    static class UrlCountPair {
        String url;
        int count;

        public UrlCountPair(String url, int count) {
            this.url = url;
            this.count = count;
        }

        public int getCount() {
            return count;
        }

        @Override
        public String toString()
        {
            return url + '\t' + count;
        }
    }
}
