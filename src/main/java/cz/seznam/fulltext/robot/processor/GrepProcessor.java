package cz.seznam.fulltext.robot.processor;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GrepProcessor
{
    public void process(String regex) throws IOException
    {
        Pattern pattern = Pattern.compile(regex);

        try(BufferedReader input = new BufferedReader(new InputStreamReader(System.in)))
        {
            String line;
            while ((line = input.readLine()) != null) {
                Matcher matcher = pattern.matcher(line);
                if (matcher.find()) {
                    System.out.println(line);
                }
            }
        }
    }
}
