package cz.seznam.fulltext.robot.processor;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class ContentTypeProcessor implements SingleArgumentProcessor
{
    @Override
    public void process() throws IOException
    {
        Map<String, Integer> contentTypeCount = new HashMap<>();

        try(BufferedReader input = new BufferedReader(new InputStreamReader(System.in)))
        {
            String line;
            while ((line = input.readLine()) != null) {
                String contentType = line.split("\t")[1];
                contentTypeCount.put(contentType, contentTypeCount.getOrDefault(contentType, 0) + 1);
            }
        }

        new TreeMap<>(contentTypeCount).forEach((contentType, count) -> System.out.println(contentType + "\t" + count));
    }
}
