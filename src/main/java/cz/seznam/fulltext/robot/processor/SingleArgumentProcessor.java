package cz.seznam.fulltext.robot.processor;

import java.io.IOException;

public interface SingleArgumentProcessor
{
    void process() throws IOException;
}
