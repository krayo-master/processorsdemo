# processorsdemo

**Build application:**
mvn clean install

**Run application:**
 As stated in the assignment with the exception that command has to include targetClassesPath ->
 _cat **inputFilePath** | java -classpath **targetClassesPath** cz.seznam.fulltext.robot.Runner Top_

Notes:
I had some troubles with running Mockito (it probably requires some additional configuration if not used with spring-boot dependencies, not sure.)